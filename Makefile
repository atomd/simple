version = 2.7
python = python$(version)


.pip.log: env/bin/python
	env/bin/pip install -r requirements.txt --log .pip.log

venv/bin/python:
	virtualenv-$(version) --no-site-packages --distribute venv
	@touch $@

clean:
	@rm -rfv venv/

rmpyc:
	@rm -rfv   `find   jiantuwx   -name   *.pyc`

.PHONY: clean rmpyc
