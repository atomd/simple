from setuptools import setup, find_packages

import jiantuwx
from email.utils import parseaddr

flask_requires = [
    'Flask >= 0.10.1',
    'Flask-Script',
]

install_requires = [
]

dependency_links = [
]

install_requires.extend(flask_requires)
author, author_email = parseaddr(jiantuwx.__author__)

setup(
    name='jiantu-wx',
    version=jiantuwx.__version__,
    classifiers=[], # Get strings from http://pypi.python.org/pypi?%3Aaction=list_classifiers
    author=author,
    author_email=author_email,
    url='http://jiantuapp.com/',
    packages=find_packages(exclude=['ez_setup', 'examples', 'tests']),
    include_package_data=True,
    zip_safe=False,
    install_requires=install_requires,
    dependency_links = dependency_links,
    entry_points="""
    # -*- Entry points: -*-
    """,
)
