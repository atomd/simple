# -*- coding: utf-8 -*-

from flask import Flask, render_template, request
app = Flask(__name__, template_folder='templates')
app.debug =True

@app.route("/")
def index():
    return "index page"

@app.route('/upload',  methods=['POST', 'GET'])
def upload():
    print 'test1'
    if request.method == 'POST':
        print 'test2'
        try:
            limit = request.form.get('limit')
            file = request.files['photo']
            if limit or file:
                limit = int(limit)
                filename = secure_filename(file.filename)
                file.save(os.path.join('/tmp', filename))
                file_url = url_for('.uploaded_file', filename=filename)
                share_url = url_for('.share', id=photo.name)
                return jsonify({'photo_url': file_url, 'share_url': share_url})
        except:
            pass
        return jsonify({'result': 'error'})

    print 'test'
    return render_template('upload.html')

@app.route('/uploads/<filename>')
def uploaded_file(filename):
    return send_from_directory('/tmp', filename)

@app.route("/share")
def share():
    return 'share page'

if __name__ == "__main__":
    app.run()
